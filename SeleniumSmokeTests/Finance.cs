using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.ConstrainedExecution;
using OpenQA.Selenium.Interactions;
using System.Reflection.Metadata.Ecma335;

namespace SeleniumSmokeTests
{
    class Finance
    {

        public static string test_grouping = "Finance";

        public static void finCredits(IWebDriver driver, int smokeTestRunId)
        {
            //string test_grouping = "Finance";
            string code = "E2E288";
            int smoke_test_id = MySQL.sqlTestQuery(Finance.test_grouping, code);

            var watch = System.Diagnostics.Stopwatch.StartNew();

            // return driver to homepage
            driver.Navigate().GoToUrl("https://thirteen-qa0.solutions-saadian.com/ang");

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            // click on Profile and Household subheader
            driver.FindElement(By.LinkText("Profile & Household")).Click();

            // navigate to My finances by selecting dropped down
            int handledException = 0;
            string handledExceptionInfo = "";
            try
            {
                Actions actFinance = new Actions(driver);
                IWebElement elFinance = driver.FindElement(By.LinkText("My finances"));
                actFinance.MoveToElement(elFinance).Click().Perform();
                driver.FindElement(By.LinkText("My finances")).Click(); // requires maximise other link is stale/weird - are both of these needed?
            }
            catch (Exception ex)
            {
                handledException = 1;
                handledExceptionInfo = code + " - 'My finances' click failed (" + ex.GetType().Name + ")";
                driver.Navigate().GoToUrl("https://thirteen-qa0.solutions-saadian.com/ang/customer/my-finance/summary");
            }


            //driver.FindElement(By.XPath("//label[span[contains(./span, ' Presenting my data about tenancy and finances on this website ')]]/div/input")).Click(); /// test this!
            //driver.FindElement(By.Id("mat-checkbox-2-input")).Click();

            // navigate to Charges submenu
            driver.FindElement(By.LinkText("Payments / Credits")).Click();

            watch.Stop();
            var test_time = watch.ElapsedMilliseconds;

            if (driver.Url.Contains("my-finance/allpayments"))
            {
                if (driver.FindElements(By.XPath("//div[text() = 'This page shows all payments/credits on your accounts. Transactions can take up to 3 days to appear in your online account.']")).Count != 0) // check for existence of heading
                {
                    if (driver.FindElements(By.XPath("//span[text() = 'UPDATE']")).Count != 0)
                    { // check for existence of button - can we improve this check
                        // successful test
                        MySQL.sqlResultInsert(0, 1, code + " successful", handledException, handledExceptionInfo, test_time, smoke_test_id, smokeTestRunId);
                    }
                    else
                    {
                        MySQL.sqlResultInsert(0, 0, code + " - CREDITS button not found", handledException, handledExceptionInfo, test_time, smoke_test_id, smokeTestRunId);
                    }
                }
                else
                {
                    MySQL.sqlResultInsert(0, 0, code + " - CREDITS banner not found", handledException, handledExceptionInfo, test_time, smoke_test_id, smokeTestRunId);
                }
            }
            else
            {
                MySQL.sqlResultInsert(0, 0, code + " - CREDITS URL incorrect", handledException, handledExceptionInfo, test_time, smoke_test_id, smokeTestRunId);
            }
        }

        public static void finCharges(IWebDriver driver, int smokeTestRunId)
        {
            string code = "E2E276";
            int smoke_test_id = MySQL.sqlTestQuery(Finance.test_grouping, code);

            var watch = System.Diagnostics.Stopwatch.StartNew();

            // return driver to homepage
            driver.Navigate().GoToUrl("https://thirteen-qa0.solutions-saadian.com/ang");

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            // click on Profile and Household subheader
            driver.FindElement(By.LinkText("Profile & Household")).Click();

            // navigate to My finances by selecting dropped down
            int handledException = 0;
            string handledExceptionInfo = "";
            try
            {
                Actions actFinance = new Actions(driver);
                IWebElement elFinance = driver.FindElement(By.LinkText("My finances"));
                actFinance.MoveToElement(elFinance).Click().Perform();
                driver.FindElement(By.LinkText("My finances")).Click(); // requires maximise other link is stale/weird - are both of these needed?
            }
            catch (Exception ex)
            {
                handledException = 1;
                handledExceptionInfo = code + " - 'My finances' click failed (" + ex.GetType().Name + ")";
                driver.Navigate().GoToUrl("https://thirteen-qa0.solutions-saadian.com/ang/customer/my-finance/summary");
            }

            // navigate to Charges submenu
            driver.FindElement(By.LinkText("Charges")).Click();

            watch.Stop();
            var test_time = watch.ElapsedMilliseconds;

            if (driver.Url.Contains("my-finance/allcharges"))
            {
                if (driver.FindElements(By.XPath("//div[text() = 'This page shows all charges on your accounts. Transactions can take up to 3 days to appear in your online account.']")).Count != 0) // check for existence of heading
                {
                    if (driver.FindElements(By.XPath("//span[text() = 'UPDATE']")).Count != 0)
                    { // check for existence of button - can we improve this check
                      // successful test
                        MySQL.sqlResultInsert(0, 1, code + " successful", handledException, handledExceptionInfo, test_time, smoke_test_id, smokeTestRunId);
                    }
                    else
                    {
                        MySQL.sqlResultInsert(0, 0, code + " - CHARGES button not found", handledException, handledExceptionInfo, test_time, smoke_test_id, smokeTestRunId);
                    }
                }
                else
                {
                    MySQL.sqlResultInsert(0, 0, code + " - CHARGES banner not found", handledException, handledExceptionInfo, test_time, smoke_test_id, smokeTestRunId);
                }
            }
            else
            {
                MySQL.sqlResultInsert(0, 0, code + " - CHARGES URL incorrect", handledException, handledExceptionInfo, test_time, smoke_test_id, smokeTestRunId);
            }
        }

        public static void finSummary(IWebDriver driver, int smokeTestRunId)
        {
            string code = "E2E284"; // none actually for this so this is a placeholder
            int smoke_test_id = MySQL.sqlTestQuery(Finance.test_grouping, code);

            var watch = System.Diagnostics.Stopwatch.StartNew();

            // return driver to homepage
            driver.Navigate().GoToUrl("https://thirteen-qa0.solutions-saadian.com/ang");

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            // click on Profile and Household subheader
            driver.FindElement(By.LinkText("Profile & Household")).Click();

            // navigate to My finances by selecting dropped down
            int handledException = 0;
            string handledExceptionInfo = "";
            try
            {
                Actions actFinance = new Actions(driver);
                IWebElement elFinance = driver.FindElement(By.LinkText("My finances"));
                actFinance.MoveToElement(elFinance).Click().Perform();
                driver.FindElement(By.LinkText("My finances")).Click(); // requires maximise other link is stale/weird - are both of these needed?
            }
            catch (Exception ex)
            {
                handledException = 1;
                handledExceptionInfo = code + " - 'My finances' click failed (" + ex.GetType().Name + ")";
                driver.Navigate().GoToUrl("https://thirteen-qa0.solutions-saadian.com/ang/customer/my-finance/summary");
            }

            watch.Stop();
            var test_time = watch.ElapsedMilliseconds;

            // confirm successful load by querying certain objects
            if (driver.FindElements(By.XPath("//div[text() = ' Things due this month (and early next month) ... ']")).Count != 0) // check for existence of heading
            {
                if (driver.FindElements(By.XPath("//div[text() = ' You have the following credit ']")).Count != 0)
                {// check for existence of heading
                    if (driver.FindElements(By.XPath("//span[text() = 'MAKE A PAYMENT']")).Count != 0) // check for existence of button - can we check the link?
                    {
                        MySQL.sqlResultInsert(0, 1, code + " successful", handledException, handledExceptionInfo, test_time, smoke_test_id, smokeTestRunId);
                    }
                    else
                    {
                        MySQL.sqlResultInsert(0, 0, code + " - SUMMARY button not found", handledException, handledExceptionInfo, test_time, smoke_test_id, smokeTestRunId);
                    }
                }
                else
                {
                    MySQL.sqlResultInsert(0, 0, code + " - SUMMARY element not found", handledException, handledExceptionInfo, test_time, smoke_test_id, smokeTestRunId);
                }
            }
            else
            {
                MySQL.sqlResultInsert(0, 0, code + " - SUMMARY element not found", handledException, handledExceptionInfo, test_time, smoke_test_id, smokeTestRunId);
            }
        }

        public static void finAllTransactions(IWebDriver driver, int smokeTestRunId)
        {
            string code = "E2E284";
            int smoke_test_id = MySQL.sqlTestQuery(Finance.test_grouping, code);

            var watch = System.Diagnostics.Stopwatch.StartNew();

            // return driver to homepage
            driver.Navigate().GoToUrl("https://thirteen-qa0.solutions-saadian.com/ang");

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            // click on Profile and Household subheader
            driver.FindElement(By.LinkText("Profile & Household")).Click();

            // navigate to My finances by selecting dropped down
            int handledException = 0;
            string handledExceptionInfo = "";
            try
            {
                Actions actFinance = new Actions(driver);
                IWebElement elFinance = driver.FindElement(By.LinkText("My finances"));
                actFinance.MoveToElement(elFinance).Click().Perform();
                driver.FindElement(By.LinkText("My finances")).Click(); // requires maximise other link is stale/weird - are both of these needed?
            }
            catch (Exception ex)
            {
                handledException = 1;
                handledExceptionInfo = code + " - 'My finances' click failed (" + ex.GetType().Name + ")";
                driver.Navigate().GoToUrl("https://thirteen-qa0.solutions-saadian.com/ang/customer/my-finance/summary");
            }

            // navigate to Make a payment submenu
            driver.FindElement(By.LinkText("All transactions")).Click();

            watch.Stop();
            var test_time = watch.ElapsedMilliseconds;

            // confirm successful load by querying certain objects
            if (driver.Url.Contains("my-finance/fullbreakdown"))
            {
                if (driver.FindElements(By.XPath("//div[text() = 'This page shows all transaction on your accounts. Transactions can take up to 3 days to appear in your online account.']")).Count != 0) // check for existence of heading
                {
                    if (driver.FindElements(By.XPath("//span[text() = 'UPDATE']")).Count != 0) // check for existence of button - can we improve this check
                    {
                        MySQL.sqlResultInsert(0, 1, code + " successful", handledException, handledExceptionInfo, test_time, smoke_test_id, smokeTestRunId);
                    }
                    else
                    {
                        MySQL.sqlResultInsert(0, 0, code + " - ALL TRANS button not found", handledException, handledExceptionInfo, test_time, smoke_test_id, smokeTestRunId);
                    }
                }
                else
                {
                    MySQL.sqlResultInsert(0, 0, code + " - ALL TRANS banner not found", handledException, handledExceptionInfo, test_time, smoke_test_id, smokeTestRunId);
                }
            }
            else
            {
                MySQL.sqlResultInsert(0, 0, code + " - ALL TRANS URL incorect", handledException, handledExceptionInfo, test_time, smoke_test_id, smokeTestRunId);
            }
        }

        public static void finMakePayment(IWebDriver driver, int smokeTestRunId)
        {
            string code = "E2E292";
            int smoke_test_id = MySQL.sqlTestQuery(Finance.test_grouping, code);

            var watch = System.Diagnostics.Stopwatch.StartNew();

            // return driver to homepage
            driver.Navigate().GoToUrl("https://thirteen-qa0.solutions-saadian.com/ang");

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            
            // click on Profile and Household subheader
            driver.FindElement(By.LinkText("Profile & Household")).Click();

            // navigate to My finances by selecting dropped down
            int handledException = 0;
            string handledExceptionInfo = "";
            try
            {
                Actions actFinance = new Actions(driver);
                IWebElement elFinance = driver.FindElement(By.LinkText("My finances"));
                actFinance.MoveToElement(elFinance).Click().Perform();
                driver.FindElement(By.LinkText("My finances")).Click(); // both clicks are needed
            }
            catch(Exception ex)
            {
                handledException = 1;
                handledExceptionInfo = code + " - 'My finances' click failed (" + ex.GetType().Name + ")";
                driver.Navigate().GoToUrl("https://thirteen-qa0.solutions-saadian.com/ang/customer/my-finance/summary");
            }

            // navigate to Make a payment submenu
            driver.FindElement(By.LinkText("Make a payment")).Click();

            watch.Stop();
            var test_time = watch.ElapsedMilliseconds;

            // confirm successful load by querying certain objects
            if (driver.Url.Contains("my-finance/makeapayment"))
            {
                if (driver.FindElements(By.XPath("//div[text() = 'This page shows your financial information, including things you need to pay in the current month and anything else you owe us.']")).Count != 0) // check for existence of heading
                {
                    if (driver.FindElements(By.XPath("//span[text() = 'MAKE A PAYMENT']")).Count != 0) // check for existence of button - can we check the link?
                    {
                        MySQL.sqlResultInsert(0, 1, code + " successful", handledException, handledExceptionInfo, test_time, smoke_test_id, smokeTestRunId);
                    }
                    else
                    {
                        MySQL.sqlResultInsert(0, 0, code + " - PAYMENT button not found", handledException, handledExceptionInfo, test_time, smoke_test_id, smokeTestRunId);
                    }
                }
                else
                {
                    MySQL.sqlResultInsert(0, 0, code + " - PAYMENT element not found", handledException, handledExceptionInfo, test_time, smoke_test_id, smokeTestRunId);
                }
            }
            else
            {
                MySQL.sqlResultInsert(0, 0, code + " - PAYMENT URL incorrect", handledException, handledExceptionInfo, test_time, smoke_test_id, smokeTestRunId);
            }
        }
    }
}