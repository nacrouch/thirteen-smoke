using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.ConstrainedExecution;
using OpenQA.Selenium.Interactions;
using System.Reflection.Metadata.Ecma335;

namespace SeleniumSmokeTests
{
    class Customer
    {

        public static string test_grouping = "Customer";

        public static void custLogin(IWebDriver driver, string envUrl, string appUser, string appPass, int smokeTestRunId)
        {
            string code = "E2E2";
            //string test_grouping = "Customer";
            int smoke_test_id = MySQL.sqlTestQuery(Customer.test_grouping, code);

            var watch = System.Diagnostics.Stopwatch.StartNew();

            driver.Manage().Window.Maximize();

            driver.Navigate().GoToUrl(envUrl);

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(1);

            // If log in button needs to be clicked first 
            //driver.FindElement(By.XPath("//*[@id='flexx3ColHeader']/div[3]/div/div/flexx-button/button")).Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(5);

            var usern = driver.FindElement(By.Name("username"));
            var pass = driver.FindElement(By.Name("password"));

            for (int i = 0; i < appUser.Length; i++)
            {
                usern.SendKeys(appUser[i].ToString());
            }

            for (int i = 0; i < appPass.Length; i++)
            {
                pass.SendKeys(appPass[i].ToString());
            }

            driver.FindElement(By.Id("loginBtn")).Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(20);

            // for first time tenants - TEST THIS
            if (driver.FindElements(By.Name("confirmed")).Count != 0)
            {
                //WaitUntilElementExists(By.Name("confirmed"));
                Actions action = new Actions(driver);
                IWebElement confirm = driver.FindElement(By.Name("confirmed"));
                action.MoveToElement(confirm).Click().Perform();
                // does consent always appear or only on first login?

                // driver.FindElement(By.CssSelector("//#organisationContainer > form > flexx-simple-panel > div > div.flexx-simple-panel-body > flexx-button-wrapper > div > flexx-button > button")).Click();
                driver.FindElement(By.XPath("//*[@id='organisationContainer']/form/flexx-simple-panel/div/div[2]/flexx-button-wrapper/div/flexx-button/button")).Click();
            }

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(20);

            // Close customer only pop up
            IWebElement elPopUpClose = new WebDriverWait(driver, TimeSpan.FromSeconds(20)).Until(ExpectedConditions.ElementToBeClickable(By.XPath("//button[@matdialogclose]")));
            elPopUpClose.Click();

            // This is another option to close pop up which didnt end up working
            //IWebElement elPopUpGo = new WebDriverWait(driver, TimeSpan.FromSeconds(20)).Until(ExpectedConditions.ElementToBeClickable(By.XPath("//button[span[contains(., 'Let's Go!')]]")));
            //elPopUpGo.Click();

            watch.Stop();
            var test_time = watch.ElapsedMilliseconds;

            // need to verify this exists before proceeding
            if (driver.Url.Contains("customer/tenantDashboard/"))
            {
                MySQL.sqlResultInsert(0, 1, code + " TENANT login successful", 0, "", test_time, smoke_test_id, smokeTestRunId);
            }
            else if (driver.Url.Contains("customer/dashboard"))
            {
                MySQL.sqlResultInsert(0, 1, code + " CUSTOMER login successful", 0, "", test_time, smoke_test_id, smokeTestRunId);
            }
            else
            {
                MySQL.sqlResultInsert(0, 0, code + "- login failed", 0, "", test_time, smoke_test_id, smokeTestRunId);
            }
        }

        public static void custHouseHistory(IWebDriver driver, int smokeTestRunId)
        {
            string code = "E2E317";
            int smoke_test_id = MySQL.sqlTestQuery(Customer.test_grouping, code);

            var watch = System.Diagnostics.Stopwatch.StartNew();

            // return driver to homepage
            driver.Navigate().GoToUrl("https://thirteen-qa0.solutions-saadian.com/ang");

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            // Click on Profile and Household subheader
            driver.FindElement(By.LinkText("Profile & Household")).Click();

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);

            // Navigate to My current living situation by selecting dropdown
            int handledException = 0;
            string handledExceptionInfo = "";
            try {
                //IWebElement elCurSit = new WebDriverWait(driver, TimeSpan.FromSeconds(20)).Until(ExpectedConditions.ElementToBeClickable(By.LinkText("My current living situation")));
                Actions actCurSit = new Actions(driver);
                IWebElement elCurSit = driver.FindElement(By.LinkText("My current living situation"));
                // appears to be a duplicated action but both of these are necessary for the click to go through
                actCurSit.MoveToElement(elCurSit).Click().Perform();
                driver.FindElement(By.LinkText("My current living situation")).Click(); // 
            }
            catch (NoSuchElementException)
            {
                handledException = 1;
                handledExceptionInfo = "couldnt access 'My current living situation' manually";
                driver.Navigate().GoToUrl("https://thirteen-qa0.solutions-saadian.com/ang/customer/profile/household/current/members/1002078"); // will need to account for this
            }
            catch (StaleElementReferenceException)
            {
                handledException = 1;
                handledExceptionInfo = "couldnt access 'My current living situation' manually";
                driver.Navigate().GoToUrl("https://thirteen-qa0.solutions-saadian.com/ang/customer/profile/household/current/members/1002078"); // will need to account for this
            }

            // Navigate to Housing history submenu
            driver.FindElement(By.LinkText("Housing history")).Click();

            // Add new housing history record
            IWebElement elAddAddress = new WebDriverWait(driver, TimeSpan.FromSeconds(20)).Until(ExpectedConditions.ElementToBeClickable(By.XPath("//button[contains(concat(' ', @class, ' '), ' mat-raised-button mat-button-base flexx-button-lg flexx-button-primary ng-star-inserted ') and ./span/text() = 'ADD NEW ADDRESS ']")));
            elAddAddress.Click();

            // Add house number
            var elHouseNum = driver.FindElement(By.XPath("//div[label[contains(., ' House Number')]]/div/input"));
            string houseNum = "1";
            elHouseNum.SendKeys(houseNum);

            // Add street name
            var elStreetName = driver.FindElement(By.XPath("//div[label[contains(., ' Street Name')]]/div/input"));
            string streetName = "Test St";
            for (int i = 0; i < streetName.Length; i++)
            {
                elStreetName.SendKeys(streetName[i].ToString());
            }

            // Add town/city
            var elTownCity = driver.FindElement(By.XPath("//div[label[contains(., ' Town/City')]]/div/input"));
            string townCity = "Test City";
            for (int i = 0; i < townCity.Length; i++)
            {
                elTownCity.SendKeys(townCity[i].ToString());
            }

            // Add postcode
            var elPostcode = driver.FindElement(By.XPath("//div[label[contains(., ' Postcode')]]/div/input"));
            string postcode = "TT1 1TT";
            for (int i = 0; i < postcode.Length; i++)
            {
                elPostcode.SendKeys(postcode[i].ToString());
            }

            // Add start date
            var elStartDate = driver.FindElement(By.XPath("//div[label[contains(., ' Start Date')]]/div/input"));
            string startDate = "01/01/2020";
            for (int i = 0; i < startDate.Length; i++)
            {
                elStartDate.SendKeys(startDate[i].ToString());
            }

            // Add end date
            var elEndDate = driver.FindElement(By.XPath("//div[label[contains(., ' End Date')]]/div/input"));
            string endDate = "02/01/2020";
            for (int i = 0; i < endDate.Length; i++)
            {
                elEndDate.SendKeys(endDate[i].ToString());
            }

            // Save this record
            driver.FindElement(By.XPath("//button[contains(concat(' ', @class, ' '), ' mat-raised-button mat-button-base flexx-button-lg flexx-button-secondary ng-star-inserted ') and ./span/text() = 'SAVE']")).Click();

            watch.Stop();
            var test_time = watch.ElapsedMilliseconds;

            // Verify the test has completed before removing the added record
            if (driver.Url.Contains("current/housingHistory"))
            {
                if (driver.FindElements(By.XPath("//h2[contains(., '1 Test St, Test City, TT1')]")).Count != 0) // check for existence of heading
                {
                    // check for existence of button - can we improve this check
                    // successful test
                    MySQL.sqlResultInsert(0, 1, code + " successful", handledException, handledExceptionInfo, test_time, smoke_test_id, smokeTestRunId);
                }
                else
                {
                    MySQL.sqlResultInsert(0, 0, code + " - added HOUSE HIST element not found", handledException, handledExceptionInfo, test_time, smoke_test_id, smokeTestRunId);
                }
            }
            else
            {
                MySQL.sqlResultInsert(0, 0, code + " - HOUSE HIST URL incorrect", handledException, handledExceptionInfo, test_time, smoke_test_id, smokeTestRunId);
            }

            //delete element just created
            //driver.FindElement(By.XPath("//flexx-simple-panel/div/div/div[/div/flexx-container/div/flexx-grid-container/div/flexx-grid-item/div/flexx-h2/h2/text() = '1 Test St, Test City, TT1']/button")).Click();
            //driver.FindElement(By.XPath("//flexx-simple-panel/div/div/div[./div/flexx-container/div/flexx-grid-container/div/flexx-grid-item/div/flexx-h2/h2/text() = '1 Test St, Test City, TT1']/button")).Click();
            //driver.FindElement(By.XPath("//flexx-simple-panel/div/div/div[./div/flexx-container/div/flexx-grid-container/div/flexx-grid-item/div/flexx-h2/h2[contains(., '1 Test St, Test City, TT1')]/button")).Click();
            //driver.FindElement(By.XPath("//flexx-simple-panel/div/div/div[contains(., '1 Test St, Test City, TT1']/button")).Click();
            //*[@id="flexxMain3ColContainer"]/div[2]/div/thirteen-my-household-housing-history/flexx-container/div/thirteen-tenancy-history-panel/div/flexx-container/div/flexx-grid-container/div/flexx-container/div/flexx-simple-panel[4]/div/div[1]/div[1]/div/flexx-container/div/flexx-grid-container/div/flexx-grid-item/div/flexx-h2/h2
            //*[@id="flexxMain3ColContainer"]/div[2]/div/thirteen-my-household-housing-history/flexx-container/div/thirteen-tenancy-history-panel/div/flexx-container/div/flexx-grid-container/div/flexx-container/div/flexx-simple-panel[4]/div/div[1]/div[2]/button

            Actions actDelHistory = new Actions(driver);
            IWebElement elDelHistory = driver.FindElement(By.XPath("//h2[contains(., '1 Test St, Test City, TT1')]/ancestor::flexx-simple-panel/div/div/div/button/i"));
            actDelHistory.MoveToElement(elDelHistory).Click().Perform();
            // driver.FindElement(By.XPath("//flexx-simple-panel[4]/div/div[1]/div[2]/button/i")).Click();
            driver.FindElement(By.XPath("//button[contains(., 'Delete')]")).Click();
            driver.FindElement(By.XPath("//button[contains(concat(' ', @class, ' '), ' mat-raised-button mat-button-base flexx-button-md flexx-button-secondary ng-star-inserted ') and ./span/text() = ' CONFIRM ']")).Click();

            // will need to handle exception here
            new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.UrlContains("housingHistory"));
        }

        public static void custHouseResident(IWebDriver driver, int smokeTestRunId) // this needs a delete operation on the added resident - half finished as cant currently save residents
        {
            string code = "E2E273";
            int smoke_test_id = MySQL.sqlTestQuery(Customer.test_grouping, code);

            var watch = System.Diagnostics.Stopwatch.StartNew();

            // return driver to homepage
            driver.Navigate().GoToUrl("https://thirteen-qa0.solutions-saadian.com/ang");

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            // click on Profile and Household subheader
            driver.FindElement(By.LinkText("Profile & Household")).Click();

            // navigate to My finances by selecting dropped down
            Actions actCurSit = new Actions(driver);
            IWebElement elCurSit = driver.FindElement(By.LinkText("My current living situation"));

            int handledException = 0;
            try
            {
                actCurSit.MoveToElement(elCurSit).Click().Perform();
                driver.FindElement(By.LinkText("My current living situation")).Click(); // requires maximise other link is stale/weird - are both of these needed?
                                                                                        // this sometimes fails if load is too quick
            }
            catch (NoSuchElementException)
            {
                handledException = 1;
                driver.Navigate().GoToUrl("https://thirteen-qa0.solutions-saadian.com/ang/customer/profile/household/current/members/1002078"); // will need to account for this
            }
            catch (StaleElementReferenceException)
            {
                handledException = 1;
                driver.Navigate().GoToUrl("https://thirteen-qa0.solutions-saadian.com/ang/customer/profile/household/current/members/1002078"); // will need to account for this
            }

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            driver.FindElement(By.XPath("//flexx-button[contains(concat(' ', @class, ' '), ' add-a-resident-button ng-star-inserted ')]")).Click();

            // not sure if this only picks first of the dropdowns on this page
            driver.FindElement(By.XPath("//div[contains(concat(' ', @class, ' '), ' mat-select-arrow-wrapper ')]")).Click();
            driver.FindElement(By.XPath("//mat-option[contains(concat(' ', @class, ' '), ' mat-option ng-star-inserted ') and ./span/text() = ' Mr ']")).Click();

            // var elFirstName = driver.FindElement(By.XPath("//div[contains(concat(' ', @class, ' '), ' mat-form-field-infix ') and ./label/text() = ' First name']"));
            //var elFirstName = driver.FindElement(By.XPath("//input[contains(concat(' ', @class, ' '), ' flexx-text-input mat-input-element mat-form-field-autofill-control cdk-text-field-autofill-monitored ng-pristine ng-invalid ng-touched ') and ../label/text() = ' First name']"));
            var elFirstName = driver.FindElement(By.XPath("//div[label[contains(., ' First name')]]/div/input"));

            string firstName = "Test";

            for (int i = 0; i < firstName.Length; i++)
            {
                elFirstName.SendKeys(firstName[i].ToString());
            }

            var elLastName = driver.FindElement(By.XPath("//div[label[contains(., ' Last name')]]/div/input"));

            string lastName = "Test";

            for (int i = 0; i < lastName.Length; i++)
            {
                elLastName.SendKeys(lastName[i].ToString());
            }

            driver.FindElement(By.XPath("//div[label[contains(., ' Relationship to Main tenant')]]/mat-select")).Click();
            driver.FindElement(By.XPath("//mat-option[contains(concat(' ', @class, ' '), ' mat-option ng-star-inserted ') and ./span/text() = ' Brother ']")).Click();

            var elDateBirth = driver.FindElement(By.XPath("//div[label[contains(., ' Date Of Birth')]]/div/input"));

            string dateBirth = "01/01/2000";

            for (int i = 0; i < dateBirth.Length; i++)
            {
                elDateBirth.SendKeys(dateBirth[i].ToString());
            }

            IWebElement elScroll = driver.FindElement(By.XPath("//div[label[contains(., ' Sleeps in')]]/div/input"));
            //int posScroll = elScroll.GetProperty;
            //String js = String.Format("window.scroll(0, %s)", posScroll);
            //((IJavaScriptExecutor)driver).ExecuteScript(js);
            //Actions actScroll = new Actions(driver);
            //actScroll.MoveToElement(elScroll);
            //actScroll.Perform(); // trying to get the pop up to scroll down to the other fields

            driver.FindElement(By.XPath("//div[label[contains(., ' Sleeps in')]]/mat-select")).Click();
            driver.FindElement(By.XPath("//mat-option[contains(concat(' ', @class, ' '), ' mat-option ng-star-inserted ') and ./span/text() = ' Room 1 ']")).Click();

            driver.FindElement(By.XPath("//div[label[contains(., ' Sex')]]/mat-select")).Click();
            driver.FindElement(By.XPath("//mat-option[contains(concat(' ', @class, ' '), ' mat-option ng-star-inserted ') and ./span/text() = ' Male ']")).Click();

            var elDateMoveIn = driver.FindElement(By.XPath("//div[label[contains(., ' Date moved in ')]]/div/input"));

            string dateMoveIn = "01/01/2020";

            for (int i = 0; i < dateMoveIn.Length; i++)
            {
                elDateBirth.SendKeys(dateMoveIn[i].ToString());
            }

            driver.FindElement(By.Id("saveButton")).Click();
        }
    }
}