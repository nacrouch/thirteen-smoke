using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.ConstrainedExecution;
using OpenQA.Selenium.Interactions;
using System.Reflection.Metadata.Ecma335;

namespace SeleniumSmokeTests
{
    class Officer
    {

        public static string test_grouping = "Officer";

        public static void offLogin(IWebDriver driver, string envUrl, string appUser, string appPass, int smokeTestRunId)
        {
            string code = "E2E13"; // view tenancy details

            int smoke_test_id = MySQL.sqlTestQuery(Officer.test_grouping, code);

            var watch = System.Diagnostics.Stopwatch.StartNew();

            driver.Manage().Window.Maximize();
            //  var searchB = driver.FindElement(By.XPath("//*[@id='flexx3ColHeader']/div[3]/div/div/flexx-button/button/span"));

            var nextUrl = "";

            nextUrl = envUrl; // + "/searchManager";
            driver.Navigate().GoToUrl(nextUrl);

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(1);

            // not all env driver.FindElement(By.XPath("//*[@id='flexx3ColHeader']/div[3]/div/div/flexx-button/button")).Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(5);

            //WaitUntilElementExists(By.Name("username"));

            var usern = driver.FindElement(By.Name("username"));
            var pass = driver.FindElement(By.Name("password"));

            for (int i = 0; i < appUser.Length; i++)
            {
                usern.SendKeys(appUser[i].ToString());
            }

            for (int i = 0; i < appPass.Length; i++)
            {
                pass.SendKeys(appPass[i].ToString());
            }

            driver.FindElement(By.Id("loginBtn")).Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(20);

            IWebElement elGoButton = new WebDriverWait(driver, TimeSpan.FromSeconds(20)).Until(ExpectedConditions.ElementToBeClickable(By.Id("go-button")));
            elGoButton.Click();// doesnt always have enough time to find?

            watch.Stop();
            long test_time = watch.ElapsedMilliseconds;

            // need to verify this exists before proceeding
            if (driver.Url.Contains("colleague/search/propertyAndTenancy"))
            {
                MySQL.sqlResultInsert(0, 1, code + " - successful", 0, "", test_time, smoke_test_id, smokeTestRunId);
            }
            else
            {
                MySQL.sqlResultInsert(0, 0, code + " - officer login failed", 0, "", test_time, smoke_test_id, smokeTestRunId);
            }
        }

        public static void offHouseSearch(IWebDriver driver, int smokeTestRunId)
        {
            string code = "HS1"; // view tenancy details

            int smoke_test_id = MySQL.sqlTestQuery(Officer.test_grouping, code);

            var watch = System.Diagnostics.Stopwatch.StartNew();
            long test_time = 0;

            // return driver to homepage
            driver.Navigate().GoToUrl("https://thirteen-qa0.solutions-saadian.com/ang");

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            // click on Profile and Household subheader
            driver.FindElement(By.LinkText("Households")).Click();

            //var elHouseName = driver.FindElement(By.XPath("//input[contains(@placeholder, 'Search by first or last name...')]"));
            IWebElement elHouseName = new WebDriverWait(driver, TimeSpan.FromSeconds(20)).Until(ExpectedConditions.ElementToBeClickable(By.XPath("//input[contains(@placeholder, 'Search by first or last name...')]")));
            // use this where possible

            string houseFirstName = "daryl"; // make sure first result is the wanted one

            for (int i = 0; i < houseFirstName.Length; i++)
            {
                elHouseName.SendKeys(houseFirstName[i].ToString());
            }

            // need this click for the search to refresh, otherwise we cant confirm
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
            elHouseName.Click();
            driver.FindElement(By.XPath("//i[contains(@class, 'fas fa-search')]")).Click();

            int result = 1; // counter for result as multiple tests
            string result_info = "";
            if (driver.Url.Contains("/ang/colleague/search/personAndHousehold"))
            {
                if (driver.FindElements(By.XPath("//a[contains(text(), 'Daryl Figgis') and contains(@href, '/ang/colleague/search/personAndHousehold')]")).Count != 0) // check this works
                {
                    // successful test
                    //result = 1;
                }
                else
                {
                    result = 0;
                    result_info += " unexpected result for first name";
                }
            }
            else
            {
                result = 0;
                result_info += " incorrect URL for first name";
            }

            elHouseName.Click();
            elHouseName.Clear();

            string houseLastName = "figgis"; // make sure first result is the wanted one

            for (int i = 0; i < houseLastName.Length; i++)
            {
                elHouseName.SendKeys(houseLastName[i].ToString());
            }

            // need to get the click to happen so the search refreshes
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
            elHouseName.Click();
            driver.FindElement(By.XPath("//i[contains(@class, 'fas fa-search')]")).Click();

            if (driver.Url.Contains("/ang/colleague/search/personAndHousehold"))
            {
                if (driver.FindElements(By.XPath("//a[contains(text(), 'Daryl Figgis') and contains(@href, '/ang/colleague/search/personAndHousehold')]")).Count != 0) // check this works
                {
                    // successful test
                    //result = 1;
                }
                else
                {
                    result = 0;
                    result_info += " unexpected result for second name";
                }
            }
            else
            {
                result = 0;
                result_info += " incorrect URL for second name";
            }

            elHouseName.Click();
            elHouseName.Clear();

            string houseFullName = "daryl figgis"; // make sure first result is the wanted one

            for (int i = 0; i < houseFullName.Length; i++)
            {
                elHouseName.SendKeys(houseFullName[i].ToString());
            }

            // need to get the click to happen so the search refreshes
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
            elHouseName.Click();
            driver.FindElement(By.XPath("//i[contains(@class, 'fas fa-search')]")).Click();

            if (driver.Url.Contains("/ang/colleague/search/personAndHousehold"))
            {
                if (driver.FindElements(By.XPath("//a[contains(text(), 'Daryl Figgis') and contains(@href, '/ang/colleague/search/personAndHousehold')]")).Count != 0) // check this works
                {
                    // successful test
                    //result = 1;
                }
                else
                {
                    result = 0;
                    result_info += " unexpected result for full name";
                }
            }
            else
            {
                result = 0;
                result_info += " incorrect URL for full name";
            }

            watch.Stop();
            test_time = watch.ElapsedMilliseconds;

            if (result == 1)
            {
                MySQL.sqlResultInsert(0, result, code + " successful", 0, "", test_time, smoke_test_id, smokeTestRunId);
            }
            else
            {
                MySQL.sqlResultInsert(0, result, code + " - " + result_info, 0, "", test_time, smoke_test_id, smokeTestRunId);
            }
        }

        public static void offViewTenancy(IWebDriver driver, int smokeTestRunId)
        {
            string code = "E2E266"; // view tenancy details
            
            int smoke_test_id = MySQL.sqlTestQuery(Officer.test_grouping, code);

            var watch = System.Diagnostics.Stopwatch.StartNew();
            long test_time = 0;

            // return driver to homepage
            driver.Navigate().GoToUrl("https://thirteen-qa0.solutions-saadian.com/ang");

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            // click on Profile and Household subheader
            driver.FindElement(By.LinkText("Households")).Click();

            var elHouseName = driver.FindElement(By.XPath("//input[contains(@placeholder, 'Search by first or last name...')]"));

            string houseName = "daryl"; // make sure first result is the wanted one

            for (int i = 0; i < houseName.Length; i++)
            {
                elHouseName.SendKeys(houseName[i].ToString());
            }

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            Actions actHouseOpt = new Actions(driver);
            IWebElement elHouseOpt = driver.FindElement(By.XPath("//button[contains(@class, 'flexx-card-simple-button mat-menu-trigger ng-star-inserted')]"));
            actHouseOpt.MoveToElement(elHouseOpt).Click().Perform();
            driver.FindElement(By.XPath("//button[contains(@class, 'flexx-card-simple-button mat-menu-trigger ng-star-inserted')]")).Click();

            driver.FindElement(By.XPath("//button[contains(., 'VIEW TENANCY')]")).Click();

            // verify view mode has started
            if (driver.Url.Contains("colleague/tenancyDetails/"))
            {
                if (driver.FindElements(By.XPath("//div[span[contains(., 'View mode. You are now viewing:')]]")).Count != 0) // check this works
                {
                    if (driver.FindElements(By.XPath("//button[span[contains(., 'EXIT VIEW MODE')]]")).Count != 0)
                    {
                        // successful test
                    }
                    else
                    {
                        watch.Stop();
                        test_time = watch.ElapsedMilliseconds;

                        MySQL.sqlResultInsert(0, 0, code + " - VIEW MODE button not found", 0, "", test_time, smoke_test_id, smokeTestRunId);
                        return;
                    }
                }
                else
                {
                    watch.Stop();
                    test_time = watch.ElapsedMilliseconds;

                    MySQL.sqlResultInsert(0, 0, code + " - VIEW MODE banner not found", 0, "", test_time, smoke_test_id, smokeTestRunId);
                    return;
                }
            }
            else
            {
                watch.Stop();
                test_time = watch.ElapsedMilliseconds;

                MySQL.sqlResultInsert(0, 0, code + " - incorrect URL for VIEW MODE", 0, "", test_time, smoke_test_id, smokeTestRunId);
                return;
            }

            //this doesnt work all the time - link intercepted?
            //driver.FindElement(By.LinkText("EXIT VIEW MODE")).Click();
            driver.FindElement(By.XPath("//button[span[contains(., 'EXIT VIEW MODE')]]")).Click();

            // will need to handle exception here
            new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.UrlContains("personAndHousehold"));

            watch.Stop();
            test_time = watch.ElapsedMilliseconds;

            // verify exit view mode works
            if (driver.Url.Contains("colleague/search/personAndHousehold"))
            {
                if (driver.FindElements(By.XPath("//div[span[contains(., 'View mode. You are now viewing:')]]")).Count == 0) // check this works
                {
                    if (driver.FindElements(By.XPath("//button[span[contains(., 'EXIT VIEW MODE')]]")).Count == 0)
                    {
                        // successful test
                        MySQL.sqlResultInsert(0, 1, code + " successful", 0, "", test_time, smoke_test_id, smokeTestRunId);
                    }
                    else
                    {
                        MySQL.sqlResultInsert(0, 0, code + " - unexpected VIEW MODE button", 0, "", test_time, smoke_test_id, smokeTestRunId);
                    }
                }
                else
                {
                    MySQL.sqlResultInsert(0, 0, code + " - unexpected VIEW MODE banner", 0, "", test_time, smoke_test_id, smokeTestRunId);
                }
            }
            else
            {
                MySQL.sqlResultInsert(0, 0, code + " - incorrect URL for exit VIEW MODE", 0, "", test_time, smoke_test_id, smokeTestRunId);
            }
        }

        public static void offViewFinance(IWebDriver driver, int smokeTestRunId)
        {
            string code = "E2E267"; // view finance details

            int smoke_test_id = MySQL.sqlTestQuery(Officer.test_grouping, code);

            var watch = System.Diagnostics.Stopwatch.StartNew();
            long test_time = 0;

            // return driver to homepage
            driver.Navigate().GoToUrl("https://thirteen-qa0.solutions-saadian.com/ang");

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            // click on Profile and Household subheader
            driver.FindElement(By.LinkText("Households")).Click();

            var elHouseName = driver.FindElement(By.XPath("//input[contains(@placeholder, 'Search by first or last name...')]"));

            string houseName = "daryl"; // make sure first result is the wanted one

            for (int i = 0; i < houseName.Length; i++)
            {
                elHouseName.SendKeys(houseName[i].ToString());
            }

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            Actions actHouseOpt = new Actions(driver);
            IWebElement elHouseOpt = driver.FindElement(By.XPath("//button[contains(@class, 'flexx-card-simple-button mat-menu-trigger ng-star-inserted')]"));
            actHouseOpt.MoveToElement(elHouseOpt).Click().Perform();
            driver.FindElement(By.XPath("//button[contains(@class, 'flexx-card-simple-button mat-menu-trigger ng-star-inserted')]")).Click();

            driver.FindElement(By.XPath("//button[contains(., 'VIEW FINANCES')]")).Click();

            // verify view mode has started
            if (driver.Url.Contains("colleague/customerFinance/"))
            {
                if (driver.FindElements(By.XPath("//div[span[contains(., 'View mode. You are now viewing:')]]")).Count != 0) // check this works
                {
                    if (driver.FindElements(By.XPath("//button[span[contains(., 'EXIT VIEW MODE')]]")).Count != 0)
                    {
                        // successful test
                    }
                    else
                    {
                        watch.Stop();
                        test_time = watch.ElapsedMilliseconds;

                        MySQL.sqlResultInsert(0, 0, code + " - VIEW MODE button not found", 0, "", test_time, smoke_test_id, smokeTestRunId);
                        return;
                    }
                }
                else
                {
                    watch.Stop();
                    test_time = watch.ElapsedMilliseconds;

                    MySQL.sqlResultInsert(0, 0, code + " - VIEW MODE banner not found", 0, "", test_time, smoke_test_id, smokeTestRunId);
                    return;
                }
            }
            else
            {
                watch.Stop();
                test_time = watch.ElapsedMilliseconds;

                MySQL.sqlResultInsert(0, 0, code + " - incorrect URL for VIEW MODE", 0, "", test_time, smoke_test_id, smokeTestRunId);
                return;
            }

            //this doesnt work all the time - link intercepted?
            //driver.FindElement(By.LinkText("EXIT VIEW MODE")).Click();
            driver.FindElement(By.XPath("//button[span[contains(., 'EXIT VIEW MODE')]]")).Click();

            // will need to handle exception here
            new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.UrlContains("personAndHousehold"));

            watch.Stop();
            test_time = watch.ElapsedMilliseconds;

            // verify exit view mode works
            if (driver.Url.Contains("colleague/search/personAndHousehold"))
            {
                if (driver.FindElements(By.XPath("//div[span[contains(., 'View mode. You are now viewing:')]]")).Count == 0) // check this works
                {
                    if (driver.FindElements(By.XPath("//button[span[contains(., 'EXIT VIEW MODE')]]")).Count == 0)
                    {
                        // successful test
                        MySQL.sqlResultInsert(0, 1, code + " successful", 0, "", test_time, smoke_test_id, smokeTestRunId);
                    }
                    else
                    {
                        MySQL.sqlResultInsert(0, 0, code + " - unexpected VIEW MODE button", 0, "", test_time, smoke_test_id, smokeTestRunId);
                    }
                }
                else
                {
                    MySQL.sqlResultInsert(0, 0, code + " - unexpected VIEW MODE banner", 0, "", test_time, smoke_test_id, smokeTestRunId);
                }
            }
            else
            {
                MySQL.sqlResultInsert(0, 0, code + " - incorrect URL for exit VIEW MODE", 0, "", test_time, smoke_test_id, smokeTestRunId);
            }
        }

        public static void offAccActivation(IWebDriver driver, int smokeTestRunId)
        {
            string code = "E2E300"; // verify exit mode available on each view menu

            int smoke_test_id = MySQL.sqlTestQuery(Officer.test_grouping, code);

            var watch = System.Diagnostics.Stopwatch.StartNew();

            // return driver to homepage
            driver.Navigate().GoToUrl("https://thirteen-qa0.solutions-saadian.com/ang");

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            // click on Profile and Household subheader
            driver.FindElement(By.LinkText("Account Activation")).Click();

            watch.Stop();
            var test_time = watch.ElapsedMilliseconds;

            if (driver.Url.Contains("colleague/manageCustomerAccount"))
            {
                if (driver.FindElements(By.XPath("//button[span[contains(., 'Reset Selected Password ')]]")).Count != 0) // check this works
                {
                    if (driver.FindElements(By.XPath("//span[contains(., 'SENT A LINK BUT HAVE NOT LOGGED IN')]")).Count != 0)
                    {
                        // successful test
                        MySQL.sqlResultInsert(0, 1, code + " successful", 0, "", test_time, smoke_test_id, smokeTestRunId);
                    }
                    else
                    {
                        MySQL.sqlResultInsert(0, 0, code + " - Account Filter element not found", 0, "", test_time, smoke_test_id, smokeTestRunId);
                    }
                }
                else
                {
                    MySQL.sqlResultInsert(0, 0, code + " - Reset Password button not found", 0, "", test_time, smoke_test_id, smokeTestRunId);
                }
            }
            else
            {
                MySQL.sqlResultInsert(0, 0, code + " - URL incorrect", 0, "", test_time, smoke_test_id, smokeTestRunId);
            }
        }

        public static void offViewHouse(IWebDriver driver, int smokeTestRunId)
        {
            // run two separate smoke tests in parallel
            string code = "E2E263"; // verify exit mode available on each view menu

            int smoke_test_id = MySQL.sqlTestQuery(Officer.test_grouping, code);

            var watch = System.Diagnostics.Stopwatch.StartNew();

            long test_time = 0;

            // return driver to homepage
            driver.Navigate().GoToUrl("https://thirteen-qa0.solutions-saadian.com/ang");

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            // click on Profile and Household subheader
            driver.FindElement(By.LinkText("Households")).Click();

            var elHouseName = driver.FindElement(By.XPath("//input[contains(@placeholder, 'Search by first or last name...')]"));

            string houseName = "daryl"; // make sure first result is the wanted one

            for (int i = 0; i < houseName.Length; i++)
            {
                elHouseName.SendKeys(houseName[i].ToString());
            }

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            Actions actHouseOpt = new Actions(driver);
            IWebElement elHouseOpt = driver.FindElement(By.XPath("//button[contains(@class, 'flexx-card-simple-button mat-menu-trigger ng-star-inserted')]"));
            actHouseOpt.MoveToElement(elHouseOpt).Click().Perform();
            driver.FindElement(By.XPath("//button[contains(@class, 'flexx-card-simple-button mat-menu-trigger ng-star-inserted')]")).Click();

            driver.FindElement(By.XPath("//button[contains(., 'VIEW HOUSEHOLD')]")).Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            // verify view mode has started
            int result = 1;
            string result_info = "";
            if (driver.Url.Contains("/current/members/"))//"colleague/household/" + 
            {
                if (driver.FindElements(By.XPath("//div[span[contains(., 'View mode. You are now viewing:')]]")).Count != 0) // check this works
                {
                    if (driver.FindElements(By.XPath("//button[span[contains(., 'EXIT VIEW MODE')]]")).Count != 0)
                    {
                        // successful test
                    }
                    else
                    {
                        watch.Stop(); 
                        test_time = watch.ElapsedMilliseconds;

                        MySQL.sqlResultInsert(0, 0, code + " - VIEW MODE button not found", 0, "", test_time, smoke_test_id, smokeTestRunId);
                        return;
                    }
                }
                else
                {
                    watch.Stop();
                    test_time = watch.ElapsedMilliseconds;

                    MySQL.sqlResultInsert(0, 0, code + " - VIEW MODE banner not found", 0, "", test_time, smoke_test_id, smokeTestRunId);
                    return;
                }
            }
            else
            {
                watch.Stop();
                test_time = watch.ElapsedMilliseconds;

                MySQL.sqlResultInsert(0, 0, code + " - incorrect URL for VIEW MODE", 0, "", test_time, smoke_test_id, smokeTestRunId);
                return;
            }

            driver.FindElement(By.XPath("//a[contains(., 'Finances')]")).Click();
            new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.UrlContains("customerFinance/"));
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            //verify view mode on final screen
            if (driver.Url.Contains("colleague/customerFinance/"))
            {
                if (driver.FindElements(By.XPath("//div[span[contains(., 'View mode. You are now viewing:')]]")).Count != 0) // check this works
                {
                    if (driver.FindElements(By.XPath("//button[span[contains(., 'EXIT VIEW MODE')]]")).Count != 0)
                    {
                        // successful test
                    }
                    else
                    {
                        result = 0;
                        result_info += " FINANCES - EXIT VIEW button not found";
                    }
                }
                else
                {
                    result = 0;
                    result_info += " FINANCES VIEW banner not found";
                }
            }
            else
            {
                result = 0;
                result_info += " FINANCES VIEW URL incorrect";//driver.Url;
            }

            driver.FindElement(By.XPath("//a[contains(., 'View account details')]")).Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            //verify view mode on second screen
            if (driver.Url.Contains("account-details/members/")) //"colleague/household/" + 
            {
                if (driver.FindElements(By.XPath("//div[span[contains(., 'View mode. You are now viewing:')]]")).Count != 0) // check this works
                {
                    if (driver.FindElements(By.XPath("//button[span[contains(., 'EXIT VIEW MODE')]]")).Count != 0)
                    {
                        // successful test
                    }
                    else
                    {
                        result = 0;
                        result_info += " ACCOUNT - EXIT VIEW button not found";
                    }
                }
                else
                {
                    result = 0;
                    result_info += " ACCOUNT VIEW banner not found";
                }
            }
            else
            {
                result = 0;
                result_info += " ACCOUNT VIEW URL incorrect";
            }

            // this doesnt work all the time - link intercepted?
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.ElementExists(By.XPath("//button[span[contains(., 'EXIT VIEW MODE')]]")));
            driver.FindElement(By.XPath("//button[span[contains(., 'EXIT VIEW MODE')]]")).Click();

            // will need to handle exception here
            new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.UrlContains("personAndHousehold"));

            watch.Stop();
            test_time = watch.ElapsedMilliseconds;

            // verify exit view mode works
            if (driver.Url.Contains("colleague/search/personAndHousehold"))
            {
                if (driver.FindElements(By.XPath("//div[span[contains(., 'View mode. You are now viewing:')]]")).Count == 0) // check this works
                {
                    if (driver.FindElements(By.XPath("//button[span[contains(., 'EXIT VIEW MODE')]]")).Count == 0)
                    {
                        // successful test
                        if(result_info == "")
                        {
                            result_info = " successful";
                        }

                        MySQL.sqlResultInsert(0, result, code + " - " + result_info, 0, "", test_time, smoke_test_id, smokeTestRunId);
                    }
                    else
                    {
                        result = 0;
                        result_info += " unexpected VIEW MODE button";
                        MySQL.sqlResultInsert(0, result, code + " - " + result_info, 0, "", test_time, smoke_test_id, smokeTestRunId);
                    }
                }
                else
                {
                    result = 0;
                    result_info += " unexpected VIEW MODE banner";
                    MySQL.sqlResultInsert(0, result, code + " - " + result_info, 0, "", test_time, smoke_test_id, smokeTestRunId);
                }
            }
            else
            {
                result = 0;
                result_info += " incorrect URL for exit VIEW MODE";
                MySQL.sqlResultInsert(0, result, code + " - " + result_info, 0, "", test_time, smoke_test_id, smokeTestRunId);
            }
        }

        public static void offImpersonate(IWebDriver driver, int smokeTestRunId)
        {
            string code = "E2E105";

            int smoke_test_id = MySQL.sqlTestQuery(Officer.test_grouping, code);

            var watch = System.Diagnostics.Stopwatch.StartNew();

            // return driver to homepage
            driver.Navigate().GoToUrl("https://thirteen-qa0.solutions-saadian.com/ang");

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            // click on Profile and Household subheader
            driver.FindElement(By.LinkText("Households")).Click();

            var elHouseName = driver.FindElement(By.XPath("//input[contains(@placeholder, 'Search by first or last name...')]"));

            string houseName = "daryl"; // make sure first result is the wanted one

            for (int i = 0; i < houseName.Length; i++)
            {
                elHouseName.SendKeys(houseName[i].ToString());
            }

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            Actions actHouseOpt = new Actions(driver);
            IWebElement elHouseOpt = driver.FindElement(By.XPath("//button[contains(@class, 'flexx-card-simple-button mat-menu-trigger ng-star-inserted')]"));
            actHouseOpt.MoveToElement(elHouseOpt).Click().Perform();
            driver.FindElement(By.XPath("//button[contains(@class, 'flexx-card-simple-button mat-menu-trigger ng-star-inserted')]")).Click();

            driver.FindElement(By.XPath("//button[contains(., 'ACT ON BEHALF OF CUSTOMER')]")).Click();

            driver.FindElement(By.LinkText("My tenancy")).Click();

            // need to verify with multiple tenancies
            watch.Stop();
            var test_time = watch.ElapsedMilliseconds;

            if (driver.Url.Contains("customer/my-current-tenancy"))
            {
                if (driver.FindElements(By.XPath("//div[contains(text(), 'You are currently acting on behalf of')]")).Count != 0) // check this works
                {
                    if (driver.FindElements(By.XPath("//button[span[contains(., 'Stop acting on behalf of']")).Count != 0)
                    {
                        // successful test
                        MySQL.sqlResultInsert(0, 1, code + " successful", 0, "", test_time, smoke_test_id, smokeTestRunId);
                    }
                    else
                    {
                        MySQL.sqlResultInsert(0, 0, code + " - IMPERSONATION button not found", 0, "", test_time, smoke_test_id, smokeTestRunId);
                    }
                }
                else
                {
                    MySQL.sqlResultInsert(0, 0, code + " - IMPERSONATION banner not found", 0, "", test_time, smoke_test_id, smokeTestRunId);
                }
            }
            else
            {
                MySQL.sqlResultInsert(0, 0, code + " - IMPERSONATION URL incorrect", 0, "", test_time, smoke_test_id, smokeTestRunId);
            }

            // separate smoke test to exit impersonation so reset variables
            code = "E2E257";

            smoke_test_id = MySQL.sqlTestQuery(Officer.test_grouping, code);

            watch = System.Diagnostics.Stopwatch.StartNew();

            //new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.ElementExists(By.XPath("//button[span[contains(., 'Stop acting on behalf of')]]")));
            new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.UrlContains("my-current-tenancy"));
            driver.FindElement(By.XPath("//button[span[contains(., 'Stop acting on behalf of')]]")).Click();

            // didnt work without this but is a good check?
            new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.UrlContains("personAndHousehold"));

            watch.Stop();
            test_time = watch.ElapsedMilliseconds;

            if (driver.Url.Contains("colleague/search/personAndHousehold"))
            {
                // successful test 2
                if (driver.FindElements(By.XPath("//div[contains(text(), 'You are currently acting on behalf of')]")).Count == 0) // check this works
                {
                    if (driver.FindElements(By.XPath("//button[span[contains(., 'Stop acting on behalf of']")).Count == 0)
                    {
                        MySQL.sqlResultInsert(0, 1, code + " successful", 0, "", test_time, smoke_test_id, smokeTestRunId);
                    }
                    else
                    {
                        MySQL.sqlResultInsert(0, 0, code + " - unexpected IMPERSONATION button", 0, "", test_time, smoke_test_id, smokeTestRunId);
                    }
                }
                else
                {
                    MySQL.sqlResultInsert(0, 0, code + " - unexpected IMPERSONATION banner", 0, "", test_time, smoke_test_id, smokeTestRunId);
                }
            }
            else
            {
                MySQL.sqlResultInsert(0, 0, code + " - exit IMPERSONATION URL incorrect", 0, "", test_time, smoke_test_id, smokeTestRunId);
            }
        }

    }
}
