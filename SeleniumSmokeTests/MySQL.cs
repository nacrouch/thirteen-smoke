using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.ConstrainedExecution;
using OpenQA.Selenium.Interactions;
using System.Reflection.Metadata.Ecma335;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace SeleniumSmokeTests
{
    class MySQL
    {
        // static void Main(string[] args)
        // 

        public static void sqlRunInsert(int runResult, int runTime, string actions, string furtherComments) //string envUrl, string chromeDriverPath, string appUser, string appPass
        {

            //string connString = ConfigurationManager.ConnectionStrings["default"].ConnectionString;
            string connString = "Server=thirteenqa3-db.ccg16qhqoazm.eu-west-2.rds.amazonaws.com;Database=thirteen_staging_delta;Uid=thirteentest;Pwd=5tcG5Feyfnhj564H7BNwWZPrrw2";
            MySqlConnection conn = new MySqlConnection(connString);
            conn.Open();
            MySqlCommand comm = conn.CreateCommand();
            comm.CommandText = "INSERT INTO smoke_test_run(run_date, run_result, run_time, actions, further_comments) VALUES(@run_date, @run_result, @run_time, @actions, @further_comments)";
            comm.Parameters.Add("@run_date", MySqlDbType.VarChar).Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            comm.Parameters.Add("@run_result", MySqlDbType.VarChar).Value = runResult;
            comm.Parameters.Add("@run_time", MySqlDbType.VarChar).Value = runTime;
            comm.Parameters.Add("@actions", MySqlDbType.VarChar).Value = actions;
            comm.Parameters.Add("@further_comments", MySqlDbType.VarChar).Value = furtherComments;
            comm.ExecuteNonQuery();
            conn.Close();
        }

        public static void sqlResultInsert(int numRepeats, int result, string resultInfo, int handledException, string handledExceptionInfo, long test_time, int smoke_test_id, int smokeTestRunId) //string envUrl, string chromeDriverPath, string appUser, string appPass
        {

            //string connString = ConfigurationManager.ConnectionStrings["default"].ConnectionString;
            string connString = "Server=thirteenqa3-db.ccg16qhqoazm.eu-west-2.rds.amazonaws.com;Database=thirteen_staging_delta;Uid=thirteentest;Pwd=5tcG5Feyfnhj564H7BNwWZPrrw2";
            MySqlConnection conn = new MySqlConnection(connString);
            conn.Open();
            MySqlCommand comm = conn.CreateCommand();
            comm.CommandText = "INSERT INTO smoke_test_result(test_time, num_repeats, result, result_info, handled_exception, handled_exception_info, smoke_test_id, smoke_test_run_id) VALUES(@test_time, @num_repeats, @result, @result_info, @handled_exception, @handled_exception_info, @smoke_test_id, @smoke_test_run_id)";
            comm.Parameters.Add("@test_time", MySqlDbType.VarChar).Value = test_time;//DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            comm.Parameters.Add("@num_repeats", MySqlDbType.VarChar).Value = numRepeats;
            comm.Parameters.Add("@result", MySqlDbType.VarChar).Value = result;
            comm.Parameters.Add("@result_info", MySqlDbType.VarChar).Value = resultInfo;
            comm.Parameters.Add("@handled_exception", MySqlDbType.VarChar).Value = handledException;
            comm.Parameters.Add("@handled_exception_info", MySqlDbType.VarChar).Value = handledExceptionInfo;
            comm.Parameters.Add("@smoke_test_id", MySqlDbType.VarChar).Value = smoke_test_id;
            comm.Parameters.Add("@smoke_test_run_id", MySqlDbType.VarChar).Value = smokeTestRunId;
            comm.ExecuteNonQuery();
            conn.Close();
        }

        public static void sqlRunQUpdate(int smokeTestRunId)
        {
            //string connString = ConfigurationManager.ConnectionStrings["default"].ConnectionString;
            string connString = "Server=thirteenqa3-db.ccg16qhqoazm.eu-west-2.rds.amazonaws.com;Database=thirteen_staging_delta;Uid=thirteentest;Pwd=5tcG5Feyfnhj564H7BNwWZPrrw2";
            MySqlConnection conn = new MySqlConnection(connString);
            conn.Open();
            MySqlCommand comm = conn.CreateCommand();
            comm.CommandText = "SELECT AVG(result), COALESCE(GROUP_CONCAT(IF(result = 0, result_info, NULL)), ''), COALESCE(GROUP_CONCAT(IF(handled_exception_info <> '', handled_exception_info, NULL)), ''), SUM(test_time) FROM smoke_test_result WHERE smoke_test_run_id = @smoke_test_run_id";
            comm.Parameters.Add("@smoke_test_run_id", MySqlDbType.VarChar).Value = smokeTestRunId;
            MySqlDataReader read = comm.ExecuteReader();

            int sqlResultAvg = 0;
            string sqlResultInfo = "";
            string sqlResultExcInfo = "";
            int sqlResultTime = 0;
            if (read.Read())
            {
                sqlResultAvg = read.GetInt32(read.GetOrdinal("AVG(result)"));
                sqlResultInfo = read.GetString(read.GetOrdinal("COALESCE(GROUP_CONCAT(IF(result = 0, result_info, NULL)), '')"));
                sqlResultExcInfo = read.GetString(read.GetOrdinal("COALESCE(GROUP_CONCAT(IF(handled_exception_info <> '', handled_exception_info, NULL)), '')"));
                sqlResultTime = read.GetInt32(read.GetOrdinal("SUM(test_time)"));
            }

            read.Close();

            MySqlCommand commUpd = conn.CreateCommand();
            commUpd.CommandText = "UPDATE smoke_test_run SET run_result = @run_result, run_time = @run_time, actions = @actions, further_comments = @further_comments WHERE smoke_test_run_id = @smoke_test_run_id";
            commUpd.Parameters.Add("@run_result", MySqlDbType.VarChar).Value = sqlResultAvg;
            commUpd.Parameters.Add("@run_time", MySqlDbType.VarChar).Value = sqlResultTime;
            commUpd.Parameters.Add("@actions", MySqlDbType.VarChar).Value = sqlResultInfo;
            commUpd.Parameters.Add("@further_comments", MySqlDbType.VarChar).Value = sqlResultExcInfo;
            commUpd.Parameters.Add("@smoke_test_run_id", MySqlDbType.VarChar).Value = smokeTestRunId;
            commUpd.ExecuteNonQuery();

            conn.Close();
            //return sqlRunQuery;
        }

        public static int sqlRunQuery()
        {
            //string connString = ConfigurationManager.ConnectionStrings["default"].ConnectionString;
            string connString = "Server=thirteenqa3-db.ccg16qhqoazm.eu-west-2.rds.amazonaws.com;Database=thirteen_staging_delta;Uid=thirteentest;Pwd=5tcG5Feyfnhj564H7BNwWZPrrw2";
            MySqlConnection conn = new MySqlConnection(connString);
            conn.Open();
            MySqlCommand comm = conn.CreateCommand();
            comm.CommandText = "SELECT MAX(smoke_test_run_id) FROM smoke_test_run WHERE run_date > @run_date";
            comm.Parameters.Add("@run_date", MySqlDbType.VarChar).Value = DateTime.Now.ToString("yyyy-MM-dd 00:00:00");
            MySqlDataReader read = comm.ExecuteReader();

            int sqlRunQuery = 0;
            if (read.Read())
            {
                sqlRunQuery = read.GetInt32(read.GetOrdinal("MAX(smoke_test_run_id)"));
            }

            read.Close();
            conn.Close();
            return sqlRunQuery;
        }

        public static int sqlTestQuery(string test_grouping, string code)
        {
            //string connString = ConfigurationManager.ConnectionStrings["default"].ConnectionString;
            string connString = "Server=thirteenqa3-db.ccg16qhqoazm.eu-west-2.rds.amazonaws.com;Database=thirteen_staging_delta;Uid=thirteentest;Pwd=5tcG5Feyfnhj564H7BNwWZPrrw2";
            MySqlConnection conn = new MySqlConnection(connString);
            conn.Open();
            MySqlCommand comm = conn.CreateCommand();
            comm.CommandText = "SELECT smoke_test_id FROM smoke_test WHERE test_grouping = @test_grouping AND code = @code";
            comm.Parameters.Add("@test_grouping", MySqlDbType.VarChar).Value = test_grouping;
            comm.Parameters.Add("@code", MySqlDbType.VarChar).Value = code;
            MySqlDataReader read = comm.ExecuteReader();

            int sqlTestQuery = 0;
            if (read.Read())
            {
                sqlTestQuery = read.GetInt32(read.GetOrdinal("smoke_test_id"));
            }
           
            read.Close();
            conn.Close();
            return sqlTestQuery;
        }
    }
}

/*CREATE TABLE `thirteen_staging_delta`.`smoke_test` (
  `smoke_test_id` INT NOT NULL AUTO INCREMENT,
  `test_date` DATETIME NULL,
  `num_repeats` INT NULL,
  `result` TINYINT(4) NULL,
  `result_info` VARCHAR(45) NULL,
  PRIMARY KEY (`smoke_test_id`));
*/