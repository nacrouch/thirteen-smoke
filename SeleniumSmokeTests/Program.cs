﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.ConstrainedExecution;
using OpenQA.Selenium.Interactions;
using System.Reflection.Metadata.Ecma335;

namespace SeleniumSmokeTests
{
    class Program
    {
        // static void Main(string[] args)
        // {
        private static IWebDriver driver;

        public static void Main() //string envUrl, string chromeDriverPath, string appUser, string appPass
        {
            string chromeDriverPath = @"C:\Users\Niall.Crouch\source\repos\SeleniumSmokeTests";
            string envUrl = "https://thirteen-qa0.solutions-saadian.com/ang";
            string appUser = "testofficer1@saadian.com";
            string appCustUser =  "test8@saadian.com";
            string appPass = "Thirteen2020";
            string appCustPass = "Default Password";

            driver = new ChromeDriver(chromeDriverPath);
            _ = driver.Manage().Timeouts().ImplicitWait;

            //Officer.offLogin(driver, envUrl, appUser, appPass);
            // works even if it does look a bit quick - may need to change this verification emthod Officer.offHouseSearch(driver);
            // works but need to track where th result failed Officer.offViewTenancy(driver);
            //Officer.offAccActivation(driver); // works fine as just verifying 
            // works but need to test stability Officer.offViewHouse(driver); // pretty finicky when it works
            // Officer.offImpersonate(driver);

            //logOut(driver);

            MySQL.sqlRunInsert(0, 0, "", "");
            int smokeTestRunId = MySQL.sqlRunQuery();

            //Officer.offLogin(driver, envUrl, appUser, appPass, smokeTestRunId);

            Customer.custLogin(driver, envUrl, appCustUser, appCustPass, smokeTestRunId);
            //Customer.custHouseHistory(driver, smokeTestRunId); //seems to work now? - doesnt always delete entry?
            //Tenancy.tenDataConsent(driver, smokeTestRunId);
            //Tenancy.tenAffCalculatorNot(driver, smokeTestRunId);
            //Tenancy.tenAgreementNot(driver, smokeTestRunId);
            //Tenancy.tenDashboard(driver, smokeTestRunId);
            //Tenancy.tenQuestionsNot(driver, smokeTestRunId);

            Finance.finSummary(driver, smokeTestRunId);
            Finance.finMakePayment(driver, smokeTestRunId);
            Finance.finCredits(driver, smokeTestRunId);
            Finance.finCharges(driver, smokeTestRunId);
            Finance.finAllTransactions(driver, smokeTestRunId);

            logOut(driver, smokeTestRunId);

            MySQL.sqlRunQUpdate(smokeTestRunId);

            //Customer.custHouseResident(driver); // cant get to work as scroll doesnt appear?

            // finance all editted - just need to test but pretty simple so should be fine
            // all working now but missing the check for finance/data consent - and may need variable link for catch
            //Finance.finCredits(driver);
            //Finance.finCharges(driver);
            //Finance.finAllTransactions(driver);
            //Finance.finMakePayment(driver);
            //Finance.finSummary(driver);

            //tenancy
            //works Tenancy.tenDashboard(driver);
            // works now with implicit wait Tenancy.tenAgreementNot(driver);
            // works now with implicit wait / not sure if this is a good test anyway Tenancy.tenAffCalculatorNot(driver);
            // works but may need to review the questiosn to check Tenancy.tenQuestionsNot(driver);
            // works just dont check more than 1 tenancy  - yet Tenancy.tenDataConsent(driver);

            /*driver.Manage().Window.Maximize();
            //  var searchB = driver.FindElement(By.XPath("//*[@id='flexx3ColHeader']/div[3]/div/div/flexx-button/button/span"));

            var nextUrl = "";

            nextUrl = envUrl; // + "/searchManager";
            driver.Navigate().GoToUrl(nextUrl);

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(1);

            // not all env driver.FindElement(By.XPath("//*[@id='flexx3ColHeader']/div[3]/div/div/flexx-button/button")).Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(5);

            WaitUntilElementExists(By.Name("username"));

            var usern = driver.FindElement(By.Name("username"));
            var pass = driver.FindElement(By.Name("password"));

            int i = 0;
            for (int i = 0; i < appUser.Length; i++)
            {
                usern.SendKeys(appUser[i].ToString());
            }

            for (int i = 0; i < appPass.Length; i++)
            {
                pass.SendKeys(appPass[i].ToString());
            }

            driver.FindElement(By.Id("loginBtn")).Click();

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(20);

            // for first time tenants
            if (driver.FindElements(By.Name("confirmed")).Count != 0)
            {
                WaitUntilElementExists(By.Name("confirmed"));
                Actions action = new Actions(driver);
                IWebElement confirm = driver.FindElement(By.Name("confirmed"));
                action.MoveToElement(confirm).Click().Perform();
                // does consent always appear or only on first login?

                // driver.FindElement(By.CssSelector("//#organisationContainer > form > flexx-simple-panel > div > div.flexx-simple-panel-body > flexx-button-wrapper > div > flexx-button > button")).Click();
                driver.FindElement(By.XPath("//*[@id='organisationContainer']/form/flexx-simple-panel/div/div[2]/flexx-button-wrapper/div/flexx-button/button")).Click();
            }

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(20);

            //customer only screens
            WaitUntilElementExists(By.ClassName("flexx-dialog-button-footer"));
            // WaitUntilElementClickable(By.XPath("//*[@id='mat - dialog - 1']/thirteen-account-consent-notification-dialog/flexx-dialog-container/div/div[5]/flexx-dialog-footer/flexx-button-wrapper/div/flexx-button/button"));
            //driver.FindElement(By.XPath("//*[@id='mat - dialog - 1']/thirteen-account-consent-notification-dialog/flexx-dialog-container/div/div[5]/flexx-dialog-footer/flexx-button-wrapper/div/flexx-button/button")).Click();
            //driver.FindElement(By.CssSelector("# mat-dialog-1 > thirteen-account-consent-notification-dialog > flexx-dialog-container > div > div.flexx-dialog-button-footer > flexx-dialog-footer > flexx-button-wrapper > div > flexx-button > button")).Click();
            // "/ html / body / div[4] / div[2] / div / mat - dialog - container / thirteen - account - consent - notification - dialog / flexx - dialog - container / div / div[5] / flexx - dialog - footer / flexx - button - wrapper / div / flexx - button / button"
            // "thirteen-account-consent-notification-dialog"
            //driver.FindElement(By.LinkText("LET'S GO!")).Click();

            // customer only necessary
            //driver.FindElement(By.XPath("//*[@id='mat-dialog-1']/thirteen-account-consent-notification-dialog/flexx-dialog-container/div/button")).Click();*/



            driver.Close();
            driver.Quit();
        }

        public static void logOut(IWebDriver driver, int smokeTestRunId)
        {
            string test_grouping = "User";
            string code = "E2E121"; // view finance details

            int smoke_test_id = MySQL.sqlTestQuery(test_grouping, code);

            var watch = System.Diagnostics.Stopwatch.StartNew();
            long test_time = 0;

            // return driver to homepage
            driver.Navigate().GoToUrl("https://thirteen-qa0.solutions-saadian.com/ang/colleague/dashboard");

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            //driver.FindElement(By.XPath("//*[@id='flexx3ColHeader']/div[3]/div/div/flexx-icon")).Click();

            new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.ElementToBeClickable(By.XPath("//flexx-icon[contains(concat(' ', @class, ' '), ' mat-menu-trigger ng-star-inserted ')]")));
            new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.ElementIsVisible(By.XPath("//flexx-icon[contains(concat(' ', @class, ' '), ' mat-menu-trigger ng-star-inserted ')]")));
            driver.FindElement(By.XPath("//flexx-icon[contains(concat(' ', @class, ' '), ' mat-menu-trigger ng-star-inserted ')]")).Click();
            // WaitUntilElementExists(By.LinkText("My Tenancyeret"));
            //driver.FindElement(By.XPath("//button[contains(text(), 'LOG OUT'")).Click();
            // Actions actLogOut = new Actions(driver);
            //IWebElement elLogOut = driver.FindElement(By.XPath("//button[contains(text(), 'LOG OUT'"));
            // actLogOut.MoveToElement(elLogOut).Click().Perform();
            //driver.FindElement(By.LinkText("My finances")).Click(); // do timeout with fallback to logout url
            driver.FindElement(By.XPath("//button[contains(concat(' ', @class, ' '), ' mat-menu-item ') and ./text() = 'LOG OUT']")).Click();
            driver.FindElement(By.XPath("//button[contains(concat(' ', @class, ' '), ' mat-raised-button mat-button-base flexx-button-md flexx-button-secondary ng-star-inserted ') and ./span/text() = ' YES ']")).Click();

            watch.Stop();
            test_time = watch.ElapsedMilliseconds;

            // will need to handle exception here
            new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.ElementExists(By.Name("username")));

            if (driver.Url == "https://thirteen-qa0.solutions-saadian.com/ang/")
            {
                // successful test
                MySQL.sqlResultInsert(0, 1, code + " successful", 0, "", test_time, smoke_test_id, smokeTestRunId);
            }
            else
            {
                MySQL.sqlResultInsert(0, 0, code + " - logout failed", 0, "", test_time, smoke_test_id, smokeTestRunId);
            }
        }        

        //this will search for the element until a timeout is reached
        public static IWebElement WaitUntilElementExists(By elementLocator, int timeout = 30)
        {
            try
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeout));
                return wait.Until(ExpectedConditions.ElementExists(elementLocator));
            }
            catch (NoSuchElementException)
            {
                Console.WriteLine("Element with locator: '" + elementLocator + "' was not found in current context page.");
                throw;
            }
        }
        public static IWebElement WaitUntilElementClickable(By elementLocator, int timeout = 15)
        {
            try
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeout));
                return wait.Until(ExpectedConditions.ElementToBeClickable(elementLocator));
            }
            catch (NoSuchElementException)
            {
                Console.WriteLine("Element with locator: '" + elementLocator + "' was not found in current context page.");
                throw;
            }
        }

        private static void sendKeys(IWebElement element, string text)
        {
            element.Click();
            Actions actions = new Actions(driver);
            foreach (char c in text)
            {
                actions.SendKeys(c.ToString())
                    .Perform();
            }
        }
    }
}
