using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.ConstrainedExecution;
using OpenQA.Selenium.Interactions;
using System.Reflection.Metadata.Ecma335;

namespace SeleniumSmokeTests
{
    class Tenancy
    {
        public static void tenDataConsent(IWebDriver driver, int smokeTestRunId)
        {
            string code = "E2E186";
            string test_grouping = "Tenancy";

            int smoke_test_id = MySQL.sqlTestQuery(test_grouping, code);

            var watch = System.Diagnostics.Stopwatch.StartNew();
            long test_time = 0;

            // return driver to homepage
            driver.Navigate().GoToUrl("https://thirteen-qa0.solutions-saadian.com/ang");

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            // open up manage my account
            driver.FindElement(By.XPath("//flexx-icon[contains(concat(' ', @class, ' '), ' mat-menu-trigger ng-star-inserted ')]")).Click();
            driver.FindElement(By.XPath("//button[contains(concat(' ', @class, ' '), ' mat-menu-item ') and ./text() = 'MANAGE MY ACCOUNT']")).Click();

            // open up view my data consents
            Actions actDataConsent = new Actions(driver);
            IWebElement elDataConsent = driver.FindElement(By.XPath("//button[contains(concat(' ', @class, ' '), ' mat-raised-button mat-button-base flexx-button-md ng-star-inserted ') and ./span/text() = 'VIEW MY DATA CONSENTS']"));
            actDataConsent.MoveToElement(elDataConsent).Click().Perform();
            //driver.FindElement(By.XPath("//button[contains(concat(' ', @class, ' '), ' mat-raised-button mat-button-base flexx-button-md ng-star-inserted ') and ./text() = 'VIEW MY DATA CONSENTS']")).Click();

            // verify some info here - is there a tenancy to check
            if (driver.FindElements(By.XPath("//flexx-h2/h2[text() = 'My tenancies']")).Count == 0)
            {
                watch.Stop();
                test_time = watch.ElapsedMilliseconds;
                MySQL.sqlResultInsert(0, 0, code + " - no tenancies to check", 0, "", test_time, smoke_test_id, smokeTestRunId);
                return;
            }

            // Navigate to tenancy as linked in data consents
            int handledException = 0;
            string handledExceptionInfo = "";
            try
            {
                Actions actTenancy = new Actions(driver);
                IWebElement elTenancy = driver.FindElement(By.XPath("//a[text() = 'Tenancy']"));
                actTenancy.MoveToElement(elTenancy).Click().Perform();
                driver.FindElement(By.LinkText("Tenancy")).Click(); // requires maximise other link is stale/weird - are both of these needed?
            }
            catch (NoSuchElementException)
            {
                handledException = 1;
                handledExceptionInfo = "couldnt access 'Tenancy' link manually";
                driver.Navigate().GoToUrl("https://thirteen-qa0.solutions-saadian.com/ang/customer/my-current-tenancy/key-information?customerId=1002076&tenancyId=1061"); // will need to account for this
            }
            catch (StaleElementReferenceException)
            {
                handledException = 1;
                handledExceptionInfo = "couldnt access 'Tenancy' link manually";
                driver.Navigate().GoToUrl("https://thirteen-qa0.solutions-saadian.com/ang/customer/my-current-tenancy/key-information?customerId=1002076&tenancyId=1061"); // will need to account for this
            }

            watch.Stop();
            test_time = watch.ElapsedMilliseconds;

            // need to verify with multiple tenancies???
            if (driver.Url.Contains("customer/my-current-tenancy"))
            {
                if (driver.FindElements(By.XPath("//div[text() = 'This page shows information about your tenancy. If you have (or have had) multiple tenancies you can view these using the drop down selector.']")).Count != 0) // check for existence of heading
                {
                    // successful test
                    MySQL.sqlResultInsert(0, 1, code + " successful", handledException, handledExceptionInfo, test_time, smoke_test_id, smokeTestRunId);
                }
                else
                {
                    MySQL.sqlResultInsert(0, 0, code + " - tenancy banner not found", handledException, handledExceptionInfo, test_time, smoke_test_id, smokeTestRunId);
                }
            }
            else
            {
                MySQL.sqlResultInsert(0, 0, code + " - URL incorrect", handledException, handledExceptionInfo, test_time, smoke_test_id, smokeTestRunId);
            }
        }

        public static void tenQuestionsNot(IWebDriver driver, int smokeTestRunId) // unsure how to verify this now so just confirm make a payment is present
        {
            string code = "E2E262";
            string test_grouping = "Tenancy";

            int smoke_test_id = MySQL.sqlTestQuery(test_grouping, code);

            var watch = System.Diagnostics.Stopwatch.StartNew();

            // return driver to homepage
            driver.Navigate().GoToUrl("https://thirteen-qa0.solutions-saadian.com/ang");

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            // click on Profile and Household subheader
            driver.FindElement(By.LinkText("Profile & Household")).Click();

            // navigate to My finances by selecting dropped down
            int handledException = 0;
            string handledExceptionInfo = "";
            try
            {
                Actions actTenancy = new Actions(driver);
                IWebElement elTenancy = driver.FindElement(By.LinkText("My tenancy"));
                actTenancy.MoveToElement(elTenancy).Click().Perform();
                driver.FindElement(By.LinkText("My tenancy")).Click(); // requires maximise other link is stale/weird - are both of these needed?
            }
            catch (NoSuchElementException)
            {
                handledException = 1;
                handledExceptionInfo = "couldnt access 'My tenancy' manually";
                driver.Navigate().GoToUrl("https://thirteen-qa0.solutions-saadian.com/ang/customer/my-current-tenancy/key-information?customerId=1002076"); // will need to account for this
            }
            catch (StaleElementReferenceException)
            {
                handledException = 1;
                handledExceptionInfo = "couldnt access 'My tenancy' manually";
                driver.Navigate().GoToUrl("https://thirteen-qa0.solutions-saadian.com/ang/customer/my-current-tenancy/key-information?customerId=1002076"); // will need to account for this
            }
            catch (ElementClickInterceptedException)
            {
                handledException = 1;
                handledExceptionInfo = "couldnt access 'My tenancy' manually";
                driver.Navigate().GoToUrl("https://thirteen-qa0.solutions-saadian.com/ang/customer/my-current-tenancy/key-information?customerId=1002076");
            }

            watch.Stop();
            var test_time = watch.ElapsedMilliseconds;

            // will need to handle exception here
            new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.UrlContains("my-current-tenancy/"));

            if (driver.Url.Contains("my-current-tenancy/"))
            {
                if (driver.FindElements(By.XPath("//p[contains(., 'GOT QUESTIONS?')]")).Count == 0) // need to confirm this works
                {
                    if (driver.FindElements(By.XPath("//p[contains(., 'RAISE AN ISSUE')]")).Count == 0)
                    {
                        MySQL.sqlResultInsert(0, 1, code + " successful", handledException, handledExceptionInfo, test_time, smoke_test_id, smokeTestRunId);
                    }
                    else
                    {
                        MySQL.sqlResultInsert(0, 0, code + " - RAISE AN ISSUE button present", handledException, handledExceptionInfo, test_time, smoke_test_id, smokeTestRunId);
                    }
                }
                else
                {
                    MySQL.sqlResultInsert(0, 0, code + " - GOT QUESTIONS button present", handledException, handledExceptionInfo, test_time, smoke_test_id, smokeTestRunId);
                }
            }
            else
            {
                MySQL.sqlResultInsert(0, 0, code + " - URL incorrect", handledException, handledExceptionInfo, test_time, smoke_test_id, smokeTestRunId);
            }
        }

        public static void tenAffCalculatorNot(IWebDriver driver, int smokeTestRunId) // unsure how to verify this now so just confirm make a payment is present
        {
            string code = "E2E253";
            string test_grouping = "Tenancy";

            int smoke_test_id = MySQL.sqlTestQuery(test_grouping, code);

            var watch = System.Diagnostics.Stopwatch.StartNew();

            // return driver to homepage
            driver.Navigate().GoToUrl("https://thirteen-qa0.solutions-saadian.com/ang");

            // verify the tenancy agreement isnt shown on my current tenancy
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            // click on Profile and Household subheader
            driver.FindElement(By.LinkText("Profile & Household")).Click();

            // navigate to My finances by selecting dropped down
            int handledException = 0;
            string handledExceptionInfo = "";
            try
            {
                Actions actFinance = new Actions(driver);
                IWebElement elFinance = driver.FindElement(By.LinkText("My finances"));
                actFinance.MoveToElement(elFinance).Click().Perform();
                driver.FindElement(By.LinkText("My finances")).Click(); // requires maximise other link is stale/weird - are both of these needed?
            }
            catch (NoSuchElementException)
            {
                handledException = 1;
                handledExceptionInfo = "couldnt access 'My finances' manually";
                driver.Navigate().GoToUrl("https://thirteen-qa0.solutions-saadian.com/ang/customer/my-finance/summary"); // will need to account for this
            }
            catch (StaleElementReferenceException)
            {
                handledException = 1;
                handledExceptionInfo = "couldnt access 'My finances' manually";
                driver.Navigate().GoToUrl("https://thirteen-qa0.solutions-saadian.com/ang/customer/my-finance/summary"); // will need to account for this
            }

            //failed before due to redirect occurring too quick so implicit wait here
            new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.UrlContains("my-finance"));

            watch.Stop();
            var test_time = watch.ElapsedMilliseconds;

            // confirm successful load by querying certain objects
            if (driver.Url.Contains("customer/my-finance/summary"))
            {
                if (driver.FindElements(By.XPath("//div[text() = ' Things due this month (and early next month) ... ']")).Count != 0) // check for existence of heading
                {
                    if (driver.FindElements(By.XPath("//span[text() = 'MAKE A PAYMENT']")).Count != 0) // check for existence of button - can we check the link?
                    {
                        // successful test
                        MySQL.sqlResultInsert(0, 1, code + " successful", handledException, handledExceptionInfo, test_time, smoke_test_id, smokeTestRunId);
                    }
                    else
                    {
                        MySQL.sqlResultInsert(0, 0, code + " - MAKE A PAYMENT button not found", handledException, handledExceptionInfo, test_time, smoke_test_id, smokeTestRunId);
                    }
                }
                else
                {
                    MySQL.sqlResultInsert(0, 0, code + " - finance banner not found", handledException, handledExceptionInfo, test_time, smoke_test_id, smokeTestRunId);
                }
            }
            else
            {
                MySQL.sqlResultInsert(0, 0, code + " - URL incorrect", handledException, handledExceptionInfo, test_time, smoke_test_id, smokeTestRunId);
            }
        }

        public static void tenAgreementNot(IWebDriver driver, int smokeTestRunId)
        {
            string code = "E2E252";
            string test_grouping = "Tenancy";

            int smoke_test_id = MySQL.sqlTestQuery(test_grouping, code);

            var watch = System.Diagnostics.Stopwatch.StartNew();

            // return driver to homepage
            driver.Navigate().GoToUrl("https://thirteen-qa0.solutions-saadian.com/ang");

            // verify the tenancy agreement isnt shown on my current tenancy
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            // click on Profile and Household subheader
            driver.FindElement(By.LinkText("Profile & Household")).Click();

            // navigate to My finances by selecting dropped down
            int handledException = 0;
            string handledExceptionInfo = "";
            try
            {
                Actions actTenancy = new Actions(driver);
                IWebElement elTenancy = driver.FindElement(By.LinkText("My tenancy"));
                actTenancy.MoveToElement(elTenancy).Click().Perform();
                driver.FindElement(By.LinkText("My tenancy")).Click(); // requires maximise other link is stale/weird - are both of these needed?
            }
            catch (NoSuchElementException)
            {
                handledException = 1;
                handledExceptionInfo = "couldnt access 'My tenancy' manually";
                driver.Navigate().GoToUrl("https://thirteen-qa0.solutions-saadian.com/ang/customer/my-current-tenancy/key-information?customerId=1002076"); // will need to account for this
            }
            catch (StaleElementReferenceException)
            {
                handledException = 1;
                handledExceptionInfo = "couldnt access 'My tenancy' manually";
                driver.Navigate().GoToUrl("https://thirteen-qa0.solutions-saadian.com/ang/customer/my-current-tenancy/key-information?customerId=1002076"); // will need to account for this
            }

            //failed before due to redirect occurring too quick so implicit wait here
            new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.UrlContains("my-current-tenancy"));

            watch.Stop();
            var test_time = watch.ElapsedMilliseconds;

            if (driver.Url.Contains("my-current-tenancy"))
            {
                //if (driver.FindElements(By.XPath("//p[contains(concat(' ', @class, ' '), ' flexx-display-field-p ng-star-inserted ') and ./text() = 'Tenancy active']")).Count != 0) // other acceptable tenancy status?
                if (driver.FindElements(By.XPath("//div[label[contains(., ' Status ')]]/div/p[contains(., 'Tenancy active')]")).Count != 0) // other acceptable tenancy status?
                {
                    if (driver.FindElements(By.XPath("//label[text() = ' Tenancy ID ']")).Count != 0)
                    {
                        // successful test
                        MySQL.sqlResultInsert(0, 1, code + " successful", handledException, handledExceptionInfo, test_time, smoke_test_id, smokeTestRunId);
                    }
                    else
                    {
                        MySQL.sqlResultInsert(0, 0, code + " - Tenancy ID not found", handledException, handledExceptionInfo, test_time, smoke_test_id, smokeTestRunId);
                    }
                }
                else
                {
                    MySQL.sqlResultInsert(0, 0, code + " - Tenancy Status not active", handledException, handledExceptionInfo, test_time, smoke_test_id, smokeTestRunId);
                }
            }
            else
            {
                MySQL.sqlResultInsert(0, 0, code + " - URL incorrect", handledException, handledExceptionInfo, test_time, smoke_test_id, smokeTestRunId);
            }
        }

        public static void tenDashboard(IWebDriver driver, int smokeTestRunId)
        {
            string code = "E2E111";
            string test_grouping = "Tenancy";

            int smoke_test_id = MySQL.sqlTestQuery(test_grouping, code);

            var watch = System.Diagnostics.Stopwatch.StartNew();

            // return driver to homepage
            driver.Navigate().GoToUrl("https://thirteen-qa0.solutions-saadian.com/ang");

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            // click on Profile and Household subheader
            driver.FindElement(By.LinkText("My Dashboard")).Click();

            watch.Stop();
            var test_time = watch.ElapsedMilliseconds;

            if (driver.Url.Contains("tenantDashboard"))
            {
                if (driver.FindElements(By.Id("customerDashboardMyCalendar")).Count != 0)
                {
                    if (driver.FindElements(By.XPath("//div[text() = ' My Finances ']")).Count != 0)
                    {
                        MySQL.sqlResultInsert(0, 1, code + " successful", 0, "", test_time, smoke_test_id, smokeTestRunId);
                    }
                    else
                    {
                        MySQL.sqlResultInsert(0, 0, code + " - required Finance element not found", 0, "", test_time, smoke_test_id, smokeTestRunId);
                    }
                }
                else
                {
                    MySQL.sqlResultInsert(0, 0, code + " - required Calendar element not found", 0, "", test_time, smoke_test_id, smokeTestRunId);
                }
            }
            else
            {
                MySQL.sqlResultInsert(0, 0, code + " - URL incorrect", 0, "", test_time, smoke_test_id, smokeTestRunId);
            }
        }
    }
}